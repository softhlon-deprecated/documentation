# Architecture

## Loose thoughts

- every widget is an independent small application
- widgets can emit events (in angular style)
- widgets' configuration in stored in internal, file based, database and deployable within war
- widget's catalog is stored in git repository (or database - TBC)
- widget can be extended (in angular style)
- permissions mechanism controls displaying whole widget in application, as well as can controls internals of widgets
- application structure must be visually editable in any stage of application lifecycle
- application must be ready to move entirely to IDE tool (like Intellij Idea) and back to Core Web, if needed
- all entitlements must be manageable via Core Web
- all backend API (both platform bulit-in and customer provided) must be accessible via Core Web
- i18n is a must and all should be editable in Core Web
- application structure is stored in internal, file based, database but not deployed in war (in oppposite to widgets' configuration)
- customers' provided backend services should be able to be run in debug mode
- it should be a widgets web viewer (not designer)
- application navigation should be based on angular routing and be editable in Core Web 
